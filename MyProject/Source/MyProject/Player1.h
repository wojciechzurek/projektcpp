// Fill out your copyright notice in the Description page of Project Settings.
//first actor in game 19.10.2019 round 3d square

#pragma once

#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Player1.generated.h"

UCLASS()
class MYPROJECT_API APlayer1 : public AActor	//first object created for training
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APlayer1();
	UPROPERTY(VisibleAnywhere)		//defines actor parameters
		UStaticMeshComponent* VisualMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
