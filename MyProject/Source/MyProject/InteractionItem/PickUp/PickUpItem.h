// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Components/DecalComponent.h"
#include "PickUpPowerUp.h"
#include "TimerManager.h"
#include "PickUpItem.generated.h"

class USphereComponent;
class UDecalComponent;
class APickUpPowerUp;

UCLASS()
class MYPROJECT_API APickUpItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUpItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USphereComponent* SphereComp;		//sphere

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UDecalComponent* DecalComp;			//floor

	UPROPERTY(EditInstanceOnly, Category = "PickUp")
	TSubclassOf<APickUpPowerUp> PowerUpClass;

	void Respawn();

	APickUpPowerUp* PowerUpInstance;

	UPROPERTY(EditInstanceOnly, Category = "PickUp")
	float RespawnTime;

	FTimerHandle TimerHandle_Respawn;
	
public:	

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
};
