// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TimerManager.h"
#include "UnrealNetwork.h"
#include "PickUpPowerUp.generated.h"

UCLASS()
class MYPROJECT_API APickUpPowerUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUpPowerUp();

protected:

	UPROPERTY(EditDefaultsOnly, Category = "PowerUp")
	float PowerUpInterval;		//time between ticks

	UPROPERTY(EditDefaultsOnly, Category = "PowerUp")
	int32 TotalTicksTimes;		//how many times we can apply powerup effect

	int32 TicksProcessed;		//no one left (processed all)

	FTimerHandle TimerHandle_PowerUpTickTime;

	UFUNCTION()
	void PowerUpTick();

	//FOR SERVER
	UPROPERTY(ReplicatedUsing=OnRep_ServerPowerUpActive)
	bool bIsPowerUpActive;

	UFUNCTION()
	void OnRep_ServerPowerUpActive();

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerUp")
	void OnPowerUpStateChanged(bool bNewIsActive);
	////

public:
	
	UFUNCTION(BlueprintImplementableEvent, Category = "PowerUp")			//if is active
	void Activated(AActor* ActivateForActor);

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerUp")			//if is expired
	void Expired();

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerUp")			//after pick-up
	void AfterTicked();

	void ActivateExpired(AActor* ActivateForActor);			//for activate expired pick-ups
};
