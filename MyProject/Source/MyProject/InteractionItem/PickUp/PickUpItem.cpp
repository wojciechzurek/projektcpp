// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpItem.h"

// Sets default values
APickUpItem::APickUpItem()
{
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetSphereRadius(75.0f);
//	SphereComp->SetupAttachment(RootComponent);
	RootComponent = SphereComp;
	
	DecalComp = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComp"));
	DecalComp->SetRelativeRotation(FRotator(90, 0.0f, 0.0f));
	DecalComp->DecalSize = FVector(64, 75, 75);
	DecalComp->SetupAttachment(RootComponent);

	RespawnTime = 20.0f;

	SetReplicates(true);
}

// Called when the game starts or when spawned
void APickUpItem::BeginPlay()
{
	Super::BeginPlay();

	if(Role==ROLE_Authority)
		Respawn();
}

void APickUpItem::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (Role==ROLE_Authority && PowerUpInstance)
	{
		PowerUpInstance->ActivateExpired(OtherActor);
		PowerUpInstance = nullptr;

		//set respawn timer
		GetWorldTimerManager().SetTimer(TimerHandle_Respawn, this, &APickUpItem::Respawn, RespawnTime);
	}
	
}

void APickUpItem::Respawn()
{
	if (PowerUpClass == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("PowerUpClass is nullptr. UPDATE YOUR BP (%s)"), *GetName());		
		return;
	}
	
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	PowerUpInstance = GetWorld()->SpawnActor<APickUpPowerUp>(PowerUpClass, GetTransform(), SpawnParams);
}
