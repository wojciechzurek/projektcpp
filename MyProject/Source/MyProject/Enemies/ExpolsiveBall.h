// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/HealthComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "AI/NavigationSystemBase.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/GameEngine.h"
#include "PlayerS.h"
#include "Sound/SoundCue.h"
#include "NavigationPath.h"
#include "NavigationSystem.h"
#include "DrawDebugHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Character.h"
#include "Components/SphereComponent.h"
#include "ExpolsiveBall.generated.h"

class UHealthComponent;
class USphereComponent;
class USoundCue;

UCLASS()
class MYPROJECT_API AExpolsiveBall : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AExpolsiveBall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	UStaticMeshComponent* Mesh;
//public:
	FVector FindPathToActor();		//vector to point where actor is staying
//public:
	FVector NextPathPoint;			//player location

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	USphereComponent* SphereComp;

	UPROPERTY(EditDefaultsOnly, Category="Moving Bot")
	float MovementForce;			//force for change way of ball

	UPROPERTY(EditDefaultsOnly, Category = "Moving Bot")
	float RequiredTargetDistance;
	
	UPROPERTY(EditDefaultsOnly, Category = "Moving Bot")
	bool bIsUsingVelocityChng;			//change velocity

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	UHealthComponent* BallHealthComp;		//ball health (100p)

	UFUNCTION()
	void TakeDamage(UHealthComponent* HealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, 
		class AController* InstigatedBy, AActor* DamageCauser);		//get damage (take less health) if shot/near player

	UMaterialInstanceDynamic* MaterialInst;		//dynamic material -> pulse (if damaged) --dynamical material is pulsing

	void ExplodeAfterHit();			//get explode

	UPROPERTY(EditDefaultsOnly, Category = "Moving Bot")
		UParticleSystem* ExplosionEffect;		//muzzle effect of explosion

	bool bExploded;		//is exploded?

	bool ExplosionNearPlayer;		//is near player?
	
	UPROPERTY(EditDefaultsOnly, Category = "Moving Bot")		//radius of explosion
	float ExplosionRadius;
	
	UPROPERTY(EditDefaultsOnly, Category = "Moving Bot")		//damage if exploded
	float ExplosionDamage;

	UPROPERTY(EditDefaultsOnly, Category = "Moving Bot")		//get damage if near player (for auto-destruction)
	float SelfDamageInterval;

	FTimerHandle TimerHandleExplosion;		//time for self-destruction in auto-destruction

	void GetSelfDestruct();			//run auto-destruction

	void RefreshPathToNextTarget();
	
	UPROPERTY(EditDefaultsOnly, Category = "Moving Bot")
	USoundCue* SelfDestructSound;			//sound if auto-destruction is running and every get damage make sound
	
	UPROPERTY(EditDefaultsOnly, Category = "Moving Bot")
	USoundCue* ExplosionSound;				//sound during explosion

	FTimerHandle TimerHandle_RefreshPath;	//if blocked in the same place
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

};