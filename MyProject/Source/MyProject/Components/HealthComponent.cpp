// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "NewGameMode.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	DefaultHealth = 100;
	bIsDead = false;
	TeamNumber = 255;
	
	SetIsReplicated(true);		//for multiplayer
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	if(this->GetOwnerRole() == ROLE_Authority)		//if we are server
	{

		AActor* MyOwner = GetOwner();		//follow (synchronize)
		if (MyOwner)
		{
			MyOwner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::HandleTakeAnyDamage);		//set health
		}
	}
	Health = DefaultHealth;
}

void UHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if(Damage <= 0.0f || bIsDead)	//if death
	{
		return;
	}

	if (DamagedActor != DamageCauser && IsFriendly(DamagedActor, DamageCauser))
		return;
	
	//update health clamped
	Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);		//health can be only between min and max value

	UE_LOG(LogTemp, Log, TEXT("Health Changed: %s"), *FString::SanitizeFloat(Health))		//create log in console (for test)

	bIsDead = Health <= 0.0f;
	
	ChangeHealth.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);


	if (bIsDead)	//if dead
	{
		ANewGameMode* GMode = Cast<ANewGameMode>(GetWorld()->GetAuthGameMode());	
		if (GMode)
		{
			GMode->OnActorKilled.Broadcast(GetOwner(), DamageCauser, InstigatedBy);
		}
	}
	
};


void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, Health);		//replicate health

}

void UHealthComponent::OnRep_Health(float OldHealth)
{
	float Damage = OldHealth - Health;
	
	ChangeHealth.Broadcast(this, Health, Damage, nullptr, nullptr, nullptr);	//get damage
}

float UHealthComponent::GetHealth() const
{
	return Health;
}

bool UHealthComponent::IsFriendly(AActor* Actor1, AActor* Actor2)	//shoter and victim are friendly?
{
	if (Actor1 == nullptr || Actor2 == nullptr)
		return true;
	
	UHealthComponent* Comp1 = Cast<UHealthComponent>(Actor1->GetComponentByClass(UHealthComponent::StaticClass()));
	UHealthComponent* Comp2 = Cast<UHealthComponent>(Actor2->GetComponentByClass(UHealthComponent::StaticClass()));

	if(Comp1 == nullptr || Comp2 == nullptr)	//cause bug -> GranadeLauncher Projectile dont kill
		return true;

	return Comp1->TeamNumber == Comp2->TeamNumber;
}

void UHealthComponent::PowerUpHeal(float HealAmount)
{
	if (HealAmount <= 0.0f || Health <= 0.0f)
		return;

	Health = FMath::Clamp(Health + HealAmount, 0.0f, DefaultHealth);

	UE_LOG(LogTemp, Log, TEXT("Health Changed: +%s"), *FString::SanitizeFloat(HealAmount))

	ChangeHealth.Broadcast(this, Health, -HealAmount, nullptr, nullptr, nullptr);
}
