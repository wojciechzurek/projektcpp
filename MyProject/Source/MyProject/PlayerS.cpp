// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerS.h"

// Sets default values
APlayerS::APlayerS()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//camera 3rd person
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->bUsePawnControlRotation = true;
	SpringArmComp->SetupAttachment(RootComponent);
	
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp, USpringArmComponent::SocketName);
	CameraComp->SetActive(true);

	CameraRightPosition = FVector(20, 30, 70);
	CameraLeftPosition = FVector(20, -20, 70);
	bIsCameraRightSide = true;
	CameraComp->SetRelativeLocation(CameraRightPosition);
	
	//CameraComp->bUsePawnControlRotation = true;
	//AutoPossessPlayer = EAutoReceiveInput::Player0;

	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;	//enable crouching
	
	//load mesh below
//	VisualMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PlayerMesh"));
//	VisualMesh->SetupAttachment(RootComponent);
	//load mesh to container
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> MeshContainer(TEXT("SkeletalMesh'/Game/AnimStarterPack/UE4_Mannequin/Mesh/SK_Mannequin'"));

	if(MeshContainer.Succeeded())		//if loaded corectly
	{
		GetMesh()->SetSkeletalMesh(MeshContainer.Object);	//get mesh to capsule
		GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, -88.0f));		//location
		GetMesh()->SetRelativeRotation(FRotator(0.0f, 270.0f, 0.0f));		//rotation
		GetMesh()->SetOwnerNoSee(false);		//for no bugs
		//PlayerSMesh=GetMesh();
		//PlayerSMesh->SetSkeletalMesh(MeshContainer.Object);
		
	}
	//checkf(VisualAsset.Succeeded(), TEXT("Failed to find the mesh"));
	//auto msh = AStaticMeshActor::GetStaticMeshComponent();
	//checkf(msh, TEXT("Failed to find mesh component"));
	//msh->SetStaticMesh(VisualAsset.Object);
//	if(VisualAsset.Succeeded())
//	{
//		
//		VisualMesh->SetStaticMesh(VisualAsset.Object);
//		VisualMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
//	}

	//load animation

	//static ConstructorHelpers::FObjectFinder<UAnimSequence> StayAnim(TEXT("AnimSequence'/Game/AnimStarterPack/Idle_Rifle_Hip.Idle_Rifle_Hip'"));
	//Anim =StayAnim.Object;
	//bool bLoop = false;
	//GetMesh()->PlayAnimation(Anim, bLoop);

	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);	//projectile ignore owner capsule

	HealthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComp"));	//create health

	
	SprintSpeed = 2.0f;
	SprintActive = false;	//for disable right-left-backward move
	IsAiming = false;
	AimSpeed = 0.5f;
	ZoomFOV = 30.0f;
	ZoomSpeed = 20.0f;
	WeaponAttachSocketName = "WeaponSocket";
	GranadeLauncherSocketName = "WeaponSocket2";
	Type = "Player";
	Singleplayer = true;
}

// Called when the game starts or when spawned
void APlayerS::BeginPlay()
{
	
	Super::BeginPlay();

	DefaultFOV = CameraComp->FieldOfView;		//get default field of view
	HealthComp->ChangeHealth.AddDynamic(this, &APlayerS::OnHealthChanged);		//set default health (100%)
	
	if (GEngine)		//is character in use?
	{
		//-1=never refresh message, 5=5sec.
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("PlayerS is using"));
	}

	if (Role == ROLE_Authority)
	{
		//SPAWN DEFAULT WEAPON BELOW
		FActorSpawnParameters SpawnParams;		//default weapon params
		FActorSpawnParameters SpawnParams2;		//default weapon params
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams2.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		Weapon1 = GetWorld()->SpawnActor<AWeapon01>(StarterWeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		Weapon2 = GetWorld()->SpawnActor<AGranadeLauncher>(GranadeLauncherClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams2);
		if (Weapon2)
		{
			Weapon2->SetOwner(this);		//set weapon owner
			Weapon2->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, GranadeLauncherSocketName);		//add to hand (transform and location)
			Weapon2->SetActorHiddenInGame(true);		//hide
		}

		if (Weapon1)
		{
			Weapon1->SetOwner(this);		//set weapon owner
			Weapon1->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponAttachSocketName);		//add to hand (transform and location)
			Weapon1->SetActorHiddenInGame(false);
		}
		CurrentWeapon = Weapon1;
		//	CurrentWeapon = Weapon1;

		//	CurrentWeapon = GetWorld()->SpawnActor<AWeapon01>(StarterWeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);		//spawn weapon
	}

//	HealthComp->ChangeHealth.AddDynamic(this, &APlayerS::OnHealthChanged);//>>??

	GameState = GetWorld()->GetGameState<AGameState1>();
	

}

// Called every frame
void APlayerS::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CameraComp) {
		CurrentFOV = IsAiming ? ZoomFOV : DefaultFOV;		//which zoom (is zooming?)
		FOV = FMath::FInterpTo(CameraComp->FieldOfView, CurrentFOV, DeltaTime, ZoomSpeed);	//zoom location (speed) -Interpolate function must be in loop

		CameraComp->SetFieldOfView(FOV);		//update camera for aiming
	}
	if(HealthComp->TeamNumber == 1 && Singleplayer==true)		//is singleplayer? (sprint activate)
	{

		GameState->HowManyPlayers(Type);
		if (GameState->PlayerCount != 1)
			Singleplayer = false;
		else
			Singleplayer = true;

	}
}

// Called to bind functionality to input
void APlayerS::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//movement reaction
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerS::MoveForward);	//receive inputs from project settings	//keyboard WS
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerS::MoveRight);		//keyboard AD
	PlayerInputComponent->BindAxis("Turn", this, &APlayerS::AddControllerYawInput);		//mouse x input
	PlayerInputComponent->BindAxis("Look_up", this, &APlayerS::AddControllerPitchInput);		//mouse y input
	
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlayerS::StartJump);		//start jumping
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &APlayerS::StopJump);		//stop jumping
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &APlayerS::Crouchs);		//crouch
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerS::StartSprint);		//start sprint
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerS::StopSprint);		//stop sprint
	PlayerInputComponent->BindAction("AimButton", IE_Pressed, this, &APlayerS::StartAiming);		//start aiming
	PlayerInputComponent->BindAction("AimButton", IE_Released, this, &APlayerS::StopAiming);		//stop aiming
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APlayerS::StartShot);		//start shoting
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &APlayerS::StopShot);		//stop shoting
	PlayerInputComponent->BindAction("ChangeWeapon", IE_Pressed, this, &APlayerS::ChangeWeapon);		//change weapon
	PlayerInputComponent->BindAction("CameraSwitch", IE_Pressed, this, &APlayerS::CameraSwitch);		//camera switch
}

void APlayerS::MoveForward(float Value)
{
	//do if receive MoveForward reaction	bug -> fatal error
	if (!Controller)
		return;
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);	//check axis

//	if (Direction)
//	{
	if (SprintActive == false)
		AddMovementInput(Direction, Value);		//make move
	else
		AddMovementInput(Direction, abs(Value));
//	}
}

void APlayerS::MoveRight(float Value)
{
	//do if receive MoveRight reaction
	if (SprintActive == false) {
		if (!Controller)
			return;
		FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void APlayerS::StartJump()
{
	if (GetCharacterMovement()->IsCrouching()) UnCrouch();
	bPressedJump = true;		//built-in function
}

void APlayerS::StopJump()
{
	bPressedJump = false;
}

void APlayerS::Crouchs()
{
	GetCharacterMovement()->IsCrouching() ? UnCrouch() : Crouch();
}

void APlayerS::StartSprint()
{
	if (!Singleplayer)
		return;
	
	SprintActive = true;
	if(GetCharacterMovement()->IsCrouching()) 
		UnCrouch();

	//only for Singleplayer
	GetCharacterMovement()->MaxWalkSpeed *= SprintSpeed;

}

void APlayerS::StopSprint()	//only for singleplayer
{
//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("STOP"));
	if (!Singleplayer)
		return;
	GetCharacterMovement()->MaxWalkSpeed /= SprintSpeed;
	SprintActive = false;
}

void APlayerS::StartAiming()
{
	IsAiming=true;

	if (!Singleplayer)	//only for Singleplayer
		return;
	GetCharacterMovement()->MaxWalkSpeed *= AimSpeed;
	GetCharacterMovement()->MaxWalkSpeedCrouched *= AimSpeed;

}

void APlayerS::StopAiming()
{
	IsAiming=false;

	if (!Singleplayer)	//only for singleplayer
		return;
	GetCharacterMovement()->MaxWalkSpeed /= AimSpeed;
	GetCharacterMovement()->MaxWalkSpeedCrouched /= AimSpeed;

}

FVector APlayerS::GetPawnViewLocation() const	//follow player (for camera)
{
	if(CameraComp)
		return CameraComp->GetComponentLocation();
	return Super::GetPawnViewLocation();
}

void APlayerS::StartShot()
{
	if (CurrentWeapon)
		CurrentWeapon->StartFire();
}

void APlayerS::StopShot()
{
	if (CurrentWeapon)
		CurrentWeapon->StopFire();
}

void APlayerS::ChangeWeapon()
{
//	CurrentWeapon->SetActorHiddenInGame(true);
	CurrentWeapon->StopFire();
	if (CurrentWeapon->ID == Weapon1->ID) {
		CurrentWeapon = Weapon2;
		Weapon1->SetActorHiddenInGame(true);
		Weapon2->SetActorHiddenInGame(false);
	}
	else if(CurrentWeapon->ID == Weapon2->ID)
	{
		CurrentWeapon = Weapon1;
		Weapon1->SetActorHiddenInGame(false);
		Weapon2->SetActorHiddenInGame(true);
	}
}

void APlayerS::CameraSwitch()
{
	if (bIsCameraRightSide)
	{
		CameraComp->SetRelativeLocation(CameraLeftPosition);
		bIsCameraRightSide = false;
	}
	else if (!bIsCameraRightSide)
	{
		CameraComp->SetRelativeLocation(CameraRightPosition);
		bIsCameraRightSide = true;
	}
	else
		CameraComp->SetRelativeLocation(CameraRightPosition);
}


void APlayerS::OnHealthChanged(UHealthComponent* HealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if(Health<=0.0f && !bDied)  //get died
	{
		bDied = true;
		GetMovementComponent()->StopMovementImmediately();		//stop move
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);		//ignore other items on map
		DetachFromControllerPendingDestroy();		//stop camera rotating
		SetLifeSpan(10.0f);		//hide after 10sek
		CurrentWeapon->StopFire();

		if(Weapon1)				
			Weapon1->SetLifeSpan(5.0f);		//delete weapons

		if(Weapon2)
			Weapon2->SetLifeSpan(5.0f);

//		if (Type != "Bot")
//		{
//			AGameState1* GameState = GetWorld()->GetGameState<AGameState1>();
//			GameState->PlayerCount--;
//			InGame = false;
//		}
		
	}
}


void APlayerS::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const	//for server
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlayerS, CurrentWeapon);		//replication objects and values for other players in game
	DOREPLIFETIME(APlayerS, Weapon1);
	DOREPLIFETIME(APlayerS, Weapon2);
	DOREPLIFETIME(APlayerS, bDied);
}