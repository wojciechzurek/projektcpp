// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TimerManager.h"
#include "GameState1.h"
#include "Engine/World.h"
#include "PlayerState1.h"
//#include "Components/HealthComponent.h"
#include "NewGameMode.generated.h"

enum class EWaveState : uint8;

//Event
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnActorKilled, AActor*, VictimActor, AActor*, KillerActor, AController*, KillerController);		//killed actor, killer actor	


UCLASS()
class MYPROJECT_API ANewGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	ANewGameMode();

protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "GameModeEvent")
	void SpawnEnemies();

	void StartWave();

	void StopWave();

	void WavePreparing();

	void SpawnTimeElapse();

	void CheckWaveState();

	void CheckPlayerAlive();

	void GameOver();

	void SetWaveState(EWaveState NewState);

	void RespawnDeadPlayers();

	FTimerHandle TimerHandle_EnemySpawner;

	FTimerHandle TimerHandle_BetweenWaves;

	int32 EnemiesSpawned;

	int32 WaveEnemiesCount;

	UPROPERTY(EditDefaultsOnly, Category = "GameModeEvent")
	float TimeBetweenWaves;

public:

	virtual void StartPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(BlueprintAssignable, Category = "GameMode")
	FOnActorKilled OnActorKilled;
	
};
