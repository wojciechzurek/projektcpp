// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon01.h"
#include "GranadeLauncher.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API AGranadeLauncher : public AWeapon01
{
	GENERATED_BODY()
public:
		AGranadeLauncher();

protected:
	virtual void Shot() override;

	UPROPERTY(EditDefaultsOnly, Category = "ProjectileWeapon")		
		TSubclassOf<AActor> ProjectileClass;		//granade-projectile
};
