// Fill out your copyright notice in the Description page of Project Settings.


#include "NewGameMode.h"
#include "Components/HealthComponent.h"

ANewGameMode::ANewGameMode()
{
	
	TimeBetweenWaves = 4.0f;

	GameStateClass = AGameState1::StaticClass();			//check class for gameplay
	PlayerStateClass = APlayerState1::StaticClass();
	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 1.0f;
}

void ANewGameMode::StartWave()
{
	if(GetWorld()->GetGameState<AGameState1>())
		GetWorld()->GetGameState<AGameState1>()->WaveNumber++;
	
	WaveEnemiesCount+=2;
	
	EnemiesSpawned = 2 * WaveEnemiesCount;

	GetWorldTimerManager().SetTimer(TimerHandle_EnemySpawner, this, &ANewGameMode::SpawnTimeElapse, 1.0f, true, 0.0f);

	SetWaveState(EWaveState::WaveInProgress);
}

void ANewGameMode::SpawnTimeElapse()
{
	SpawnEnemies();

	EnemiesSpawned--;

	if (EnemiesSpawned <= 0)
		StopWave();
}

void ANewGameMode::StartPlay()
{
	Super::StartPlay();

	WavePreparing();
}

void ANewGameMode::WavePreparing()
{
//	FTimerHandle TimerHandle_BetweenWaves;
	GetWorldTimerManager().SetTimer(TimerHandle_BetweenWaves, this, &ANewGameMode::StartWave, TimeBetweenWaves, false);

	SetWaveState(EWaveState::WaitingToStart);

	RespawnDeadPlayers();
}

void ANewGameMode::StopWave()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_EnemySpawner);

	SetWaveState(EWaveState::WaitingToComplete);
	
//	WavePreparing();
}

void ANewGameMode::CheckWaveState()
{
	bool bIsWavePreparing = GetWorldTimerManager().IsTimerActive(TimerHandle_BetweenWaves);
	
	if (EnemiesSpawned > 0 || bIsWavePreparing)
		return;

	
	bool bIsAnyEnemyAlive = false;
	
	for(FConstPawnIterator i = GetWorld()->GetPawnIterator(); i; i++)
	{
		APawn* TestPawn = i->Get();
		
		if (TestPawn == nullptr || TestPawn->IsPlayerControlled())
			continue;

		UHealthComponent* HealthComp = Cast<UHealthComponent>(TestPawn->GetComponentByClass(UHealthComponent::StaticClass()));

		if (HealthComp && HealthComp->GetHealth() > 0.0f)
		{
			bIsAnyEnemyAlive = true;
			break;
		}
			
	}

	if (!bIsAnyEnemyAlive)
	{
		SetWaveState(EWaveState::WaveComplete);
		WavePreparing();
	}
}

void ANewGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CheckWaveState();
	CheckPlayerAlive();
}

void ANewGameMode::CheckPlayerAlive()
{
	for(FConstPlayerControllerIterator i = GetWorld()->GetPlayerControllerIterator(); i; i++)
	{
		APlayerController* CTRL = i->Get();
		if(CTRL && CTRL->GetPawn())
		{
			APawn* Pawn = CTRL->GetPawn();
			UHealthComponent* HealthComp = Cast<UHealthComponent>(Pawn->GetComponentByClass(UHealthComponent::StaticClass()));

			if(ensure(HealthComp) && HealthComp->GetHealth() > 0.0f)
			{
				 //player is alive
				return;
			}
		}
	}
	//no alive
	GameOver();
}

void ANewGameMode::GameOver()
{
	StopWave(); //finish match

	SetWaveState(EWaveState::GameOver);
	
	UE_LOG(LogTemp, Log, TEXT("WASTED - MISSION FAILED"));
}

void ANewGameMode::SetWaveState(EWaveState NewState)
{
	AGameState1* GS = GetGameState<AGameState1>();

	if(ensureAlways(GS))
	{
		GS->SetWaveState(NewState);
	}
}

void ANewGameMode::RespawnDeadPlayers()
{
	for (FConstPlayerControllerIterator i = GetWorld()->GetPlayerControllerIterator(); i; i++)
	{
		APlayerController* CTRL = i->Get();
		if (CTRL && CTRL->GetPawn() == nullptr)
		{
			RestartPlayer(CTRL);
		}
	}
	AGameState1* GameState = GetWorld()->GetGameState<AGameState1>();//
	GameState->PlayerCount = 0;//
}
