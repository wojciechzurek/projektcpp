// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "UnrealNetwork.h"
#include "GameState1.generated.h"

UENUM(BlueprintType)
enum class EWaveState : uint8		//enum class with wave states
{
	WaitingToStart,
	
	NextWavePreparing,

	WaveInProgress,

	WaitingToComplete,

	WaveComplete,
	
	GameOver,
};


UCLASS()
class MYPROJECT_API AGameState1 : public AGameStateBase
{
	GENERATED_BODY()

protected:

	UFUNCTION()
	void OnRep_WaveState(EWaveState OldState);

	UFUNCTION(BlueprintImplementableEvent, Category = "GameState")
	void WaveStateChanged(EWaveState NewState, EWaveState OldState);		//change old state to new state
	
//public:

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_WaveState, Category = "GameState")
	EWaveState WaveState;		//current wave state

public:

	AGameState1();

	int32 WaveNumber;
	
	void SetWaveState(EWaveState NewState);

	UPROPERTY(BlueprintReadOnly, Category = "GameState")
	uint8 PlayerCount;

	UFUNCTION()
	void HowManyPlayers(FName Type);
};
