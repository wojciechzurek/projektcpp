// Fill out your copyright notice in the Description page of Project Settings.
// Player cop 19.10.2019r

#pragma once

#include "Camera/CameraComponent.h"
#include "UnrealNetwork.h"
#include "Weapon01.h"
#include "MyProject.h"
#include "GameState1.h"
#include "Components/HealthComponent.h"
#include "GranadeLauncher.h"//
#include "Math/UnrealMathUtility.h"
#include "Components/CapsuleComponent.h"
#include "Weapon01.h"
#include "ConstructorHelpers.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/StaticMeshActor.h"//
#include "Animation/AnimSequence.h"
#include "Animation/AnimationAsset.h"
#include "Engine/Engine.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PlayerS.generated.h"

class UCameraComponent;
class USpringArmComponent;
class AWeapon01;
class UHealthComponent;

UCLASS()
class MYPROJECT_API APlayerS : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerS();
	bool SprintActive;
	bool Singleplayer;	//is player in singleplayer mode?

	//AIMING AND ZOOM
	
	UPROPERTY(EditDefaultsOnly, Category = "Player", meta=(ClampMin=0.1, ClampMax=100))		//meta = zakres
		float ZoomSpeed;

	UPROPERTY(EditDefaultsOnly, Category = "Player")
		float ZoomFOV;		//zoom during aiming

	float DefaultFOV;	//zoom without aiming
	float CurrentFOV;	//return current FOV zoom
	float FOV;			//return next FOV from interpolate

	UPROPERTY(EditAnywhere=Variables, BlueprintReadWrite, Category="Variables")
		bool IsAiming;

	UPROPERTY(VisibleAnywhere, Category=SkeletalMesh)
		USkeletalMesh* MeshContainer;		//load mesh

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Movement)
		float SprintSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
		float AimSpeed;

//	UPROPERTY(VisibleAnywhere, Category = SkeletalMesh)
//		USkeletalMeshComponent* PlayerSMesh;		//

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//CAMERA COMPONENTS
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USpringArmComponent* SpringArmComp;

	FVector CameraRightPosition;

	FVector CameraLeftPosition;

	bool bIsCameraRightSide;

	//WEAPON AND SHOTING
public:

	UPROPERTY(Replicated)
	AWeapon01* CurrentWeapon;

	UPROPERTY(Replicated)
	AWeapon01* Weapon1;

	UPROPERTY(Replicated)
	AWeapon01* Weapon2;
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Player")
		TSubclassOf<AWeapon01> StarterWeaponClass;		//create subclass of weapon class

//	UPROPERTY(EditDefaultsOnly, Category = "Player")//not used
//		TSubclassOf<AGranadeLauncher> StarterWeaponClass2;		//create subclass of weapon class
	
	UPROPERTY(EditDefaultsOnly, Category = "Player")
		TSubclassOf<AGranadeLauncher> GranadeLauncherClass;		//create subclass of weapon class
	
	UPROPERTY(VisibleDefaultsOnly, Category = "Player")
		FName WeaponAttachSocketName;

	UPROPERTY(VisibleDefaultsOnly, Category = "Player")
		FName GranadeLauncherSocketName;
public:
	UFUNCTION(BlueprintCallable, Category = "Player")
	void StartShot();

	UFUNCTION(BlueprintCallable, Category = "Player")
	void StopShot();
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components")
		UHealthComponent* HealthComp;

	UPROPERTY(Replicated, BlueprintReadWrite, Category="Player")
		bool bDied;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//MOVEMENT FUNCTION
	
	//Move to forward/backward
	UFUNCTION()		//overwrite bp
		void MoveForward(float Value);

	//Move right/left
	UFUNCTION()
		void MoveRight(float Value);

	UFUNCTION()
		void StartJump();		//create flag to start jump

	UFUNCTION()
		void StopJump();		//clear jump flags

	UFUNCTION()
		void Crouchs();

//	UPROPERTY()	//
//		UAnimSequence* Anim;	//

	UFUNCTION()
		void StartSprint();

	UFUNCTION()
		void StopSprint();

	UFUNCTION()
		void StartAiming();

	UFUNCTION()
		void StopAiming();

	UFUNCTION()
		void ChangeWeapon();

	UFUNCTION()
		void CameraSwitch();

	UFUNCTION()
		void OnHealthChanged(UHealthComponent* MyHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

//	UFUNCTION()
		void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	//---//

	virtual FVector GetPawnViewLocation() const override;

	UPROPERTY(EditDefaultsOnly, Replicated, Category = "PlayerState")
		FName Type;		//for game state (player or bot)	for playerArray

	AGameState1* GameState;
};
