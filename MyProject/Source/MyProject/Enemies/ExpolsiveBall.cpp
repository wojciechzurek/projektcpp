// Fill out your copyright notice in the Description page of Project Settings.


#include "ExpolsiveBall.h"


static int32 BallParams = 0;				//for console to on/off ball visual params
FAutoConsoleVariableRef CVARDebugBallDrawing(TEXT("game.BallParams"), BallParams, TEXT("Draw debug lines for explosive ball and other params"), ECVF_Cheat);


// Sets default values
AExpolsiveBall::AExpolsiveBall()
{
	
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//load mesh component
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	Mesh->SetCanEverAffectNavigation(false);
	Mesh->SetSimulatePhysics(true);
	RootComponent = Mesh;

	//health
	BallHealthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComp"));
	BallHealthComp->ChangeHealth.AddDynamic(this, &AExpolsiveBall::TakeDamage);
	

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));		//unvisibility sphere -> if player in sphere run auto-destruction
	SphereComp->SetSphereRadius(150);	//distance to player (for activate self-destruct/auto-destruction)
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);	//for character movement
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);		//ignore others physic items
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);		//interaction with pawn/ overlap -> can occur if blocked with other physic body
	SphereComp->SetupAttachment(RootComponent);
	
	bIsUsingVelocityChng = true;
	MovementForce = 1000;					//force for change vector of movement
	RequiredTargetDistance = 300;			//target distance for new path
	ExplosionRadius = 300;
	ExplosionDamage = 100;
	bExploded = false;
	ExplosionNearPlayer = false;
	SelfDamageInterval = 0.5f;				//time between self-destruct damage (auto-destruction)
}

// Called when the game starts or when spawned
void AExpolsiveBall::BeginPlay()
{
	Super::BeginPlay();

	if(Role == ROLE_Authority)
		NextPathPoint = FindPathToActor();		//find initial move-to ->find vector to player

//	OnActorBeginOverlap.AddDynamic(this);
}

// Called every frame
void AExpolsiveBall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if (Role == ROLE_Authority && !bExploded)		//for server nad if is not exploded
	{

		float TargetDistance = (GetActorLocation() - NextPathPoint).Size();		//distance beetwen ball and player location (target) (not actual)

		//	if(!GetActorLocation().Equals(NextPathPoint()))
		if (TargetDistance <= RequiredTargetDistance)		//if distance ball->point is smaller than required, let find new vector
		{		//is about objective destination (end of path)

			NextPathPoint = FindPathToActor();		//find path to player
			if(BallParams)
				DrawDebugString(GetWorld(), GetActorLocation(), "New path!", 0, FColor::White, 5.0f);	//show text if new path is found
		}
		else	//if is near player or the same position
		{

			FVector ForceDirect = NextPathPoint - GetActorLocation();	//set velocity value
			ForceDirect.Normalize();
			ForceDirect *= MovementForce;
			Mesh->AddForce(ForceDirect, NAME_None, bIsUsingVelocityChng);
			if(BallParams)
				DrawDebugDirectionalArrow(GetWorld(), GetActorLocation(), GetActorLocation() + ForceDirect, 32, FColor::Red, false, 0.0f, 0, 1.0f);	//draw path
		}

		if(BallParams)
			DrawDebugSphere(GetWorld(), NextPathPoint, 20, 12, FColor::Red, false, 0.0f, 1.0f);	//

	}
}

FVector AExpolsiveBall::FindPathToActor()		//get next path point
{
//	AActor* PlayerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
//	ACharacter* PlayerPawn = UGameplayStatics::GetPlayerCharacter(this, 0); //get player pawn
//	UNavigationPath* NavPath = UNavigationSystemV1::FindPathToActorSynchronously(this, GetActorLocation(), PlayerPawn);		//find path to player pawn
//	UNavigationPath* NavPath = UNavigationSystemV1::FindPathToActorSynchronously(this, GetActorLocation(), PlayerPawn);

	AActor* BestTarget = nullptr;
	float NearestDistanceToTarget = FLT_MAX;
	
	for (FConstPawnIterator i = GetWorld()->GetPawnIterator(); i; i++)
	{
		APawn* TestPawn = i->Get();

		if (TestPawn == nullptr || UHealthComponent::IsFriendly(this, TestPawn))
			continue;

		UHealthComponent* HealthComp = Cast<UHealthComponent>(TestPawn->GetComponentByClass(UHealthComponent::StaticClass()));

		if (HealthComp && HealthComp->GetHealth() > 0.0f)
		{
			float Distance = (TestPawn->GetActorLocation() - this->GetActorLocation()).Size();
			
			if(Distance < NearestDistanceToTarget)
			{
				BestTarget = TestPawn;
				NearestDistanceToTarget = Distance;
			}
		}

	}

	if(BestTarget)
	{
		UNavigationPath* NavPath = UNavigationSystemV1::FindPathToActorSynchronously(this, GetActorLocation(), BestTarget);		//find path to player pawn

		GetWorldTimerManager().ClearTimer(TimerHandle_RefreshPath);		//if blocked
		GetWorldTimerManager().SetTimer(TimerHandle_RefreshPath, this, &AExpolsiveBall::RefreshPathToNextTarget, 15.0f, false);
		
		if(NavPath && NavPath->PathPoints.Num() > 1)		//if exist
		{
			return NavPath->PathPoints[1];	//return next point
		}
	}	

	return GetActorLocation();		//if failed to find path return ball location
}

void AExpolsiveBall::TakeDamage(UHealthComponent* HealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	//pulse after hit
	if(MaterialInst==nullptr)		//set dynamical mesh comp
		MaterialInst = Mesh->CreateAndSetMaterialInstanceDynamicFromMaterial(0, Mesh->GetMaterial(0));

	if(MaterialInst)
		MaterialInst->SetScalarParameterValue("LastTimeDmg", GetWorld()->TimeSeconds);	//set last time damage -> time

	//log health
	if(BallParams)
		UE_LOG(LogTemp, Log, TEXT("%s/%s"), *FString::SanitizeFloat(Health), *GetName());

//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("explodeAfterHiiiiit"));

	if (Health <= 0)	//get explode
		ExplodeAfterHit();
}

void AExpolsiveBall::ExplodeAfterHit()		//EXPLOSION
{
//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("explodeAfterHit"));

	if (bExploded)		//don't explode more than one time
		return;

	bExploded = true;
	
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());		//spawn explosion effect

	//sound
	UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());		//spawn explosion sound

	Mesh->SetVisibility(false, true);			//hide ball mesh
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	if (Role == ROLE_Authority)		//for server
	{
		TArray<AActor*> IgnoredActors;
		IgnoredActors.Add(this);		//ignore ball for damage

		//apply damage for players in sphere
		UGameplayStatics::ApplyRadialDamage(this, ExplosionDamage, GetActorLocation(), ExplosionRadius, nullptr, IgnoredActors, this, GetInstigatorController(), true);

//		//sound
//		UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());

		if(BallParams)
			DrawDebugSphere(GetWorld(), GetActorLocation(), ExplosionRadius, 16, FColor::Blue, false, 3.0f, 0, 1.5f);

		SetLifeSpan(3.0f);		//for server (show explosion before destroy)
//		Destroy(); //destructor of object
	}
}

void AExpolsiveBall::GetSelfDestruct()		//self destruction near player
{
	UGameplayStatics::ApplyDamage(this, 50, GetInstigatorController(), this, nullptr);
}

void AExpolsiveBall::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	
	if(!ExplosionNearPlayer && !bExploded)
	{
		APlayerS* PlayerPawn = Cast<APlayerS>(OtherActor);	//cast to player

		if(PlayerPawn && !UHealthComponent::IsFriendly(this, OtherActor))		//for adv AI Bot
		{
			if (Role == ROLE_Authority)
			{
				//run GetSelfDestruct in time series
				GetWorldTimerManager().SetTimer(TimerHandleExplosion, this, &AExpolsiveBall::GetSelfDestruct, SelfDamageInterval, true, 0.0f);		//delay for run GetSelfDestruct (run of time)
			}
			
			ExplosionNearPlayer = true;

			//self-destruct sound
			UGameplayStatics::SpawnSoundAttached(SelfDestructSound, RootComponent);
		}
	}
	
}

void AExpolsiveBall::RefreshPathToNextTarget()
{
	NextPathPoint = FindPathToActor();
}