// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

//SURFACE TYPES

#define SURFACE_FLESHDEFAULT		SurfaceType1		//(blood-body)
#define SURFACE_FLESHVULNERABLE		SurfaceType2		//(blood-head)


//WEAPON TRACE CHANNELS

#define COLLISION_WEAPON ECC_GameTraceChannel1		//projectile tracer