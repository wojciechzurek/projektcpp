// Fill out your copyright notice in the Description page of Project Settings.


#include "GranadeLauncher.h"

AGranadeLauncher::AGranadeLauncher()
{
	BodyDamage = 80.0f;
	RateOfFire = 20.0f;
	ID = "W2";

	SetReplicates(true);		//replicates for multiplayer
}

void AGranadeLauncher::Shot()
{
	AActor* MyOwner = GetOwner();		//function for replication (in singleplayer)

	if (MyOwner && ProjectileClass)
	{
		FVector EyeLocation;		//camera vector
		FRotator EyeRotation;		//camera direct
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);		//get player variables for camera vector

		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);		//get location of socket name (parent=fire during shot)
//		FRotator MuzzleRotator = MeshComp->GetSocketRotation(MuzzleSocketName);		//get rotation of socket name -> replace by eye rotation

		FActorSpawnParameters SpawnParams;		//projectile spawn parameters
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;	//spawn always (if collision also)
		
		GetWorld()->SpawnActor<AActor>(ProjectileClass, MuzzleLocation, EyeRotation, SpawnParams);		//where spawn
		
		LastFireTime = GetWorld()->TimeSeconds;		//(rate of fire) for shot function
	}
}