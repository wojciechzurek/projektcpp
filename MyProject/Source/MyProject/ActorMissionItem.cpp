// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorMissionItem.h"

// Sets default values
AActorMissionItem::AActorMissionItem()
{
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	MeshComp->SetupAttachment(RootComponent);

	
	ActorType = 1;
	ActivateWave = 10;
	ActualWave = 0;
	MoveVector = FVector(0, 0, 0);
	MoveRotator = FRotator(0, 0, 0);
	bIsHide = false;
	bCanInteraction = false;
//	GameState = GetWorld()->GetGameState<AGameState1>();

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	TickInterval = 5.0f;
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = TickInterval;

}

// Called when the game starts or when spawned
void AActorMissionItem::BeginPlay()
{
	Super::BeginPlay();
	GameState = GetWorld()->GetGameState<AGameState1>();
}

// Called every frame
void AActorMissionItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ActualWave = GameState->WaveNumber;
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Wave Number%d"), ActualWave));

	if (ActivateWave == ActualWave && ActorType == 1)
		HideActor();
	
}

void AActorMissionItem::HideActor()
{
	this->SetActorHiddenInGame(true);
	this->SetActorEnableCollision(false);
	bIsHide = true;
}
