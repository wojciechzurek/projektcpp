// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "PlayerState1.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API APlayerState1 : public APlayerState
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	void AddScore(float ScoreDelta);

};
