// Fill out your copyright notice in the Description page of Project Settings.


#include "GameState1.h"
#include "Engine/World.h"
#include "Engine/Engine.h"

AGameState1::AGameState1()
{
	PlayerCount = 0;
	WaveNumber = 0;
}

void AGameState1::OnRep_WaveState(EWaveState OldState)
{
	WaveStateChanged(WaveState, OldState);		//run wave state changer
}

void AGameState1::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGameState1, WaveState);
}

void AGameState1::SetWaveState(EWaveState NewState)
{
	if(Role == ROLE_Authority)
	{
		EWaveState OldState = WaveState;
		
		WaveState = NewState;
		OnRep_WaveState(OldState);	//call on server
	}
}

void AGameState1::HowManyPlayers(FName Type)	//
{

	if (GetWorld()->GetAuthGameMode() && Type=="Player")
	{
		if (GetWorld()->GetAuthGameMode()->GetNumPlayers())
			PlayerCount = GetWorld()->GetAuthGameMode()->GetNumPlayers();
	}
	
//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("%d"),PlayerCount));
}