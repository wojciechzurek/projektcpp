// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon01.h"

static int32 WeaponParams = 0;				//for console to on/off weapon visual params
FAutoConsoleVariableRef CVARDebugWeaponDrawing(TEXT("game.WeaponParams"), WeaponParams, TEXT("Draw debug lines for weapon and other params"), ECVF_Cheat);

// Sets default values
AWeapon01::AWeapon01()
{
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));	//create object for mesh loading
	RootComponent = MeshComp;	//define transform (location, rotation, scale) / whats do during spawn -- default

	MuzzleSocketName = "MuzzleSocket";		//muzzle -> fire furing shot
	TracerTargetName = "Target";			//muzzle -> blood if target hit

	BodyDamage = 20.0f;
	RateOfFire = 500.0f;
	BulletSpreadDeg = 2.0f;
	ID = "W1";		//for weapon change function

	SetReplicates(true);		//replicates for multiplayer

	NetUpdateFrequency = 66.0f;		//for multiplayer server: values from counter-strike (default ping clamp)
	MinNetUpdateFrequency = 33.0f;
}

void AWeapon01::BeginPlay()
{
	Super::BeginPlay();

	TimeBetweenShots = 60 / RateOfFire;
}


void AWeapon01::Shot()
{
	if(Role < ROLE_Authority)	//if not server
	{
		ServerShot();		//run shot on server and return
//		return;
	}
	
	AActor* MyOwner = GetOwner();		//function for replication

	if (MyOwner) 
	{
		FVector EyeLocation;		//camera vector
		FRotator EyeRotation;		//camera direct
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);		//get player variables for camera vector

		FVector ShotDirection = EyeRotation.Vector();		//shot direction is camera vector direction

		//add spread
		float SpreadRad = FMath::DegreesToRadians(BulletSpreadDeg);
		ShotDirection = FMath::VRandCone(ShotDirection, SpreadRad, SpreadRad);
		
//		UGameplayStatics::PlaySoundAtLocation(this, ShotSound, GetActorLocation());
		
		FVector TraceEnd = EyeLocation + ShotDirection * 10000;		//distance (end point)

		FCollisionQueryParams QueryParams;		//for collision
		QueryParams.AddIgnoredActor(MyOwner);		//ignore player mesh (body)
		QueryParams.AddIgnoredActor(this);			//ignore weapon mesh (body)
		QueryParams.bTraceComplex = true;		//follow ammo
		QueryParams.bReturnPhysicalMaterial = true;
		
		FVector TracerEndPoint = TraceEnd;		//Particle "Target" parameter (smuga pocisku)

		EPhysicalSurface SurfaceType = SurfaceType_Default;
		
		FHitResult Hit;		//if hit
		if(GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, COLLISION_WEAPON, QueryParams))	//if ammo trace exist (spawn line unvisual)
		{
			AActor* HitActor = Hit.GetActor();	//return hit component

			//CHECK PHYSICAL MATERIAL (Impact effect -> surface type)

/*			EPhysicalSurface*/ SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

			float ActualDamage = BodyDamage;
			if(SurfaceType==SURFACE_FLESHVULNERABLE)		//if head-shot set bonus damage
			{
				ActualDamage *= 5.0f;
			}

			UGameplayStatics::ApplyPointDamage(HitActor, ActualDamage, ShotDirection, Hit, MyOwner->GetInstigatorController(), MyOwner, DamageType);	//add damage

			PlayImpactEffect(SurfaceType, Hit.ImpactPoint);
			
//			UParticleSystem* SelectedEffect = nullptr;
//			
//			switch(SurfaceType)
//			{
//			case SURFACE_FLESHDEFAULT:
//			case SURFACE_FLESHVULNERABLE:
//				SelectedEffect = FleshImpactEffect;
//				break;
//			default:
//				SelectedEffect = DefaultImpactEffect;
//				break;
//			}
//
//			//show impact visual effect (effect of surface type
//			if (SelectedEffect)	//if weapon has configured it
//				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedEffect, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());

			
			// ---
			
			TracerEndPoint = Hit.ImpactPoint;	//trace end in target point

			HitScanTrace.SurfaceType = SurfaceType;	
		};

		if(WeaponParams)	//if cheat are turned on
			DrawDebugLine(GetWorld(), EyeLocation, TraceEnd, FColor::White, false, 1.0f, 0, 1.0f);	//draw ammo trace


		FireVisualEffect(TracerEndPoint);		//make visual effect

		//for server
		if(Role==ROLE_Authority)	//if you are server
		{
			HitScanTrace.TraceTo = TracerEndPoint;
		}
		//---
		
		LastFireTime = GetWorld()->TimeSeconds;
	}
}

void AWeapon01::FireVisualEffect(FVector_NetQuantize const &TracerEndPoint)
{
	//fire during shot (visual effect)
	if (MuzzleEffect)	//if weapon has muzzle effect
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);

	//(smuga)
	if (TracerEffect)
	{

		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
		UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MuzzleLocation);

		UGameplayStatics::PlaySoundAtLocation(this, ShotSound, GetActorLocation());
		
		if (TracerComp)
			TracerComp->SetVectorParameter(TracerTargetName, TracerEndPoint);
	}

	//SHAKE CAMERA DURING SHOT BELOW

	APawn* MyOwner = Cast<APawn>(GetOwner());
	if(MyOwner)
	{
		APlayerController* CTRL = Cast<APlayerController>(MyOwner->GetController());		//get player controller
		if (CTRL)
			CTRL->ClientPlayCameraShake(CamShake);		//let shake
	}
		
}

void AWeapon01::StartFire()		//auto fire
{
	float FirstDelay = FMath::Max(LastFireTime + TimeBetweenShots - GetWorld()->TimeSeconds, 0.0f);		//first shot delay=0, but next not
	GetWorldTimerManager().SetTimer(TimerHandle_TimeBetweenShots, this, &AWeapon01::Shot, TimeBetweenShots, true, FirstDelay);	//run f. shot after time
}

void AWeapon01::StopFire()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_TimeBetweenShots);		//clear timer
}

void AWeapon01::ServerShot_Implementation()		//shot on server
{
	Shot();
}

bool AWeapon01::ServerShot_Validate()
{
	return true;			//is code validate? (uprawnienia)
}

void AWeapon01::OnRep_HitScanTrace()
{
	//play cosmetic fx
	FireVisualEffect(HitScanTrace.TraceTo);		//show shot on server (trace, muzzle, ...)
	PlayImpactEffect(HitScanTrace.SurfaceType, HitScanTrace.TraceTo);
}

void AWeapon01::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const		//built-in function override
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AWeapon01, HitScanTrace, COND_SkipOwner);	//send sync to every connection except owner (tracer)

}

void AWeapon01::PlayImpactEffect(EPhysicalSurface SurfaceType, FVector ImpactPoint)
{
	UParticleSystem* SelectedEffect = nullptr;

	switch (SurfaceType)
	{
	case SURFACE_FLESHDEFAULT:			//body
	case SURFACE_FLESHVULNERABLE:		//head
		SelectedEffect = FleshImpactEffect;
		UGameplayStatics::PlaySoundAtLocation(this, HitBodySound, ImpactPoint);
		break;
	default:
		SelectedEffect = DefaultImpactEffect;
		UGameplayStatics::PlaySoundAtLocation(this, HitWallSound, ImpactPoint);
		break;
	}

	//show impact visual effect (effect of surface type
	if (SelectedEffect)	//if weapon has configured it
	{
		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
		FVector ShotDirection = ImpactPoint - MuzzleLocation;
		ShotDirection.Normalize();
		
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedEffect, ImpactPoint, ShotDirection.Rotation());
	}
	
}