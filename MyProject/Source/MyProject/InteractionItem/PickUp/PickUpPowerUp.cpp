// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpPowerUp.h"

// Sets default values
APickUpPowerUp::APickUpPowerUp()
{
	PowerUpInterval = 0.0f;
	TotalTicksTimes = 0;
	TicksProcessed = 0;
	bIsPowerUpActive = false;
	
	SetReplicates(true);
}

void APickUpPowerUp::PowerUpTick()		//if player get power-ups
{
	TicksProcessed++;		//processed value

	AfterTicked();		//dont exist
	
	if (TicksProcessed >= TotalTicksTimes)
	{
		Expired();

		bIsPowerUpActive = false;
		OnRep_ServerPowerUpActive();
		
		GetWorldTimerManager().ClearTimer(TimerHandle_PowerUpTickTime);

	}
}

void APickUpPowerUp::ActivateExpired(AActor* ActivateForActor)
{
	Activated(ActivateForActor);

	bIsPowerUpActive = true;
	OnRep_ServerPowerUpActive();
	
	if (PowerUpInterval > 0.0f)
		GetWorldTimerManager().SetTimer(TimerHandle_PowerUpTickTime, this, &APickUpPowerUp::PowerUpTick, PowerUpInterval, true);
	else
		PowerUpTick();
	
}

void APickUpPowerUp::OnRep_ServerPowerUpActive()
{
	OnPowerUpStateChanged(bIsPowerUpActive);
}

void APickUpPowerUp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const	//for server
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APickUpPowerUp, bIsPowerUpActive);
}