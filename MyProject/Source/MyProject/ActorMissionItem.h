//Actor type in game:
//1 - hide in game;
//2 - move;
//3 - rotation;
//4 - interaction;
//5 - unhide
//
//activate in wave
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameState1.h"
#include "Engine/Engine.h"
#include "GameFramework/GameState.h"
#include "ActorMissionItem.generated.h"

UCLASS()
class MYPROJECT_API AActorMissionItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AActorMissionItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* MeshComp;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Details")
	uint8 ActorType;

	UPROPERTY(EditInstanceOnly, Category = "Details")
	int32 ActivateWave;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Details")
	int32 ActualWave;

	UPROPERTY(EditInstanceOnly, Category = "Details")
	FVector MoveVector;

	UPROPERTY(EditInstanceOnly, Category = "Details")
	FRotator MoveRotator;

	UPROPERTY(EditInstanceOnly, Category = "Details")
	bool bIsHide;

	UPROPERTY(EditInstanceOnly, Category = "Details")
	bool bCanInteraction;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Details")
	float TickInterval;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Details")
	AGameState1* GameState;

	UFUNCTION(BlueprintCallable, Category = "Action")
	void HideActor();
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
