// Fill out your copyright notice in the Description page of Project Settings.
//created 19.10.2019
//standard weapon for player

#pragma once

#include "DrawDebugHelpers.h"
#include "MyProject.h"
#include "UnrealNetwork.h"
#include "TimerManager.h"
#include "Components/SkeletalMeshComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "CollisionQueryParams.h"
#include "Engine/World.h"
#include "Sound/SoundCue.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon01.generated.h"

class USkeletalMeshComponent;	//class for skeletal control
class UDamageType;
class UParticleSystem;


USTRUCT() //SINGLE HITSCAN WEAPON linetrace
struct FHitScanTrace
{
	GENERATED_BODY()

public:

	UPROPERTY()
		TEnumAsByte<EPhysicalSurface> SurfaceType;

	UPROPERTY()
		FVector_NetQuantize TraceTo;		//trace end
	
};

UCLASS()
class MYPROJECT_API AWeapon01 : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeapon01();

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")	//defines actor parametes
		USkeletalMeshComponent* MeshComp;
	
public:
//	UFUNCTION(BlueprintCallable, Category="Weapon")
	virtual void Shot();		//gun fire function

	UFUNCTION(Server, Reliable, WithValidation)		//shot on server
		void ServerShot();

	FName ID;		//name for weapon change function
	
protected:
	void FireVisualEffect(FVector_NetQuantize const &TracerEndPoint);		//function for muzzle effect

	void PlayImpactEffect(EPhysicalSurface SurfaceType, FVector ImpactPoint);

	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Weapon")
		TSubclassOf<UDamageType> DamageType;		//hit point damage

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category="Weapon")
		FName MuzzleSocketName;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		FName TracerTargetName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		UParticleSystem* MuzzleEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		UParticleSystem* DefaultImpactEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		UParticleSystem* FleshImpactEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		UParticleSystem* TracerEffect;
	
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		TSubclassOf<UCameraShake> CamShake;			//for camera shake during shot

	UPROPERTY(ReplicatedUsing=OnRep_HitScanTrace)
		FHitScanTrace HitScanTrace;		//analising projectile track

	UFUNCTION()
		void OnRep_HitScanTrace();

	FTimerHandle TimerHandle_TimeBetweenShots;

	float LastFireTime;

	float TimeBetweenShots;

	///SOUND

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Sound")
		USoundCue* ShotSound;			//shot sound

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Sound")
		USoundCue* HitBodySound;			//shot sound

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Sound")
		USoundCue* HitWallSound;			//shot sound

public:

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		float BodyDamage;							//take damage if hit body
	
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		float RateOfFire;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon", meta=(ClampMin = 0.0f))
		float BulletSpreadDeg;
	
	void StartFire();

	void StopFire();
};
