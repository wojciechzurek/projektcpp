// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealNetwork.h"
#include "UnrealMathUtility.h"
#include "GameFramework/Actor.h"
//#include "NewGameMode.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

//on health changed event
DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(FOnHealthCHangedSignature, UHealthComponent*, HealthComp, float, Health, float, HealthDelta, const class UDamageType*, DamageType, class AController*, InstigatedBy, AActor*, DamageCauser);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(EditDefaultsOnly, Replicated, BlueprintReadOnly, Category = "HealthComponent")
	uint8 TeamNumber;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	bool bIsDead;
	
	UPROPERTY(ReplicatedUsing=OnRep_Health, BlueprintReadOnly, Category = "HealthComponent")
	float Health;		//actual health in game

	UFUNCTION()
	void OnRep_Health(float OldHealth);

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "HealthComponent")
	float DefaultHealth;		//health during spawning

	UFUNCTION()		//get damage (injure)
	void HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:

	UPROPERTY(BlueprintAssignable, Category="Events")
	FOnHealthCHangedSignature ChangeHealth;

	float GetHealth() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HealthComponent")
	static bool IsFriendly(AActor* Actor1, AActor* Actor2);

	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	void PowerUpHeal(float HealAmount);
};
